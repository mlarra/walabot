const mongoose = require('mongoose');
const { Schema } = mongoose;

const couponsSchema = new Schema({
    servicio: String,
    link: String,
});

mongoose.model('cupones', couponsSchema);