const {WebhookClient} = require('dialogflow-fulfillment');

const mongoose = require('mongoose');
const Demand = mongoose.model('demand');
const Coupon = mongoose.model('cupones');
const Registration = mongoose.model('registration');

module.exports = app => {
    app.post('/', async (req, res) => {
        const agent = new WebhookClient({ request: req, response: res });

        function snoopy(agent) {
            agent.add(`Welcome to my Snoopy fulfillment!`);
        }

        async function registration(agent) {

            const registration = new Registration({
                name: agent.parameters.name,
                address: agent.parameters.address,
                phone: agent.parameters.phone,
                email: agent.parameters.email,
                dateSent: Date.now()
            });
            try{
                let reg = await registration.save();
                console.log(reg);
            } catch (err){
                console.log(err);
            }
        }

        async function servicio(agent) {

            Demand.findOne({'servicio': agent.parameters.servicio}, function(err, servicio) {
                if(servicio !== null){
                    servicio.counter++;
                    servicio.save();
                } else {
                    const demand = new Demand({servicio: agent.parameters.servicio});
                    demand.save();
                }
            });
            let responseText = `Quieres saber sobre ${agent.parameters.servicio}. 
                                Te dejo un link sobre mis servicios: http://bllogistics.cl`;
            
            let coupon = await Coupon.findOne({'servicio': agent.parameters.servicio})
            
            if(coupon !== null){
                responseText = `Hola! tenemos mas sobre: ${agent.parameters.servicio}. 
                Te dejo un link para un cupon de descuento:${coupon.link}`;
            }
        
            agent.add(responseText);
        }

        function fallback(agent) {
            agent.add(`no entiendo`);
            agent.add(`I'm sorry, can you try again?`);
        }

        let intentMap = new Map();
        intentMap.set('snoopy', snoopy);
        intentMap.set('servicios', servicio);
        intentMap.set('recomendacion - yes', registration);
        intentMap.set('Default Fallback Intent', fallback);

        agent.handleRequest(intentMap);
    });

}