import React, { Component } from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Landing from '../pages/Landing';
import Cotiza from '../pages/Cotiza';
import Shop from '../shop/Shop';
import Header from '../shop/Header';
import Chatbot from '../chatbot/Chatbot';

import './app.css'
import Footer from '../shop/Footer';

class App extends Component {
    render() {
        return (
            <div>
               
                <BrowserRouter>
                <div className="headerContainer">
                <Header/>
                <Route exact path="/" component={Landing}/>
                <Route exact path="/cotiza" component={Cotiza}/>
                <Route exact path="/shop" component={Shop}/>
                <Footer/>
                <Chatbot/>
                </div>
                </BrowserRouter>
                
            </div>
        )
    }
}

export default App;