import React, { Component } from 'react';
import LogoAbstract from '../../assets/images/logoAbstract.png';
import LogoCostaSur from '../../assets/images/logoCostasur.png'
import LogoCasaMorera from '../../assets/images/logoCasamorera.png'


import './landing.css'
class Landing extends Component {
    render() {
        return (
            <div style={{width:'100%'}}>
            <div className="landingCon">
                <div className="landingTitle0">Páginas Web, Chatbot, aplicaciones Web, Asesorías TI</div>
                <div className="landingButtons"><div className="landingButton">Cotiza!</div> <div className="landingButton">Hablemos</div></div>

            </div>
                <div className="landing1">
                    <div className="landingTitle">Clientes: <img src={LogoCasaMorera} alt ="chatchile"/><img src={LogoCostaSur} alt ="chatchile"/><img src={LogoAbstract} alt ="chatchile"/> BLLogistics.cl </div>
                    
                    </div>

                <div className="landing2">

                    <div className="landing2Title">Construimos paginas web en Wordpress y en React (diseño personalizado)</div>

                </div>          

                <div className="landing1">
                    <div className="landing1Title">
                    Chatea con WalaBot, es el demo de lo que podría tener tu empresa. Se integra a bases de datos y te ayuda con el servicio al cliente. Guía al usuario para que encuentre lo que esta buscando. Ayuda a llenar formularios para que el cliente tenga major interación con la marca.
                    
                    </div>
                </div>

                <div className="landing3">

                    <div className="landing2Title">Tienes una aplicacion soñada? <br/>
                    Aqui la podemos hacer! Cramos Apps para emopresas y hacemos asesorias para actualizar tu sistema Tecnológico</div>

                </div>  

                <div className="landing1">
                    <div className="landing1Title">
                    Contáctanos!
                    
                    </div>
                </div>
            </div>
        )
    }
}

export default Landing
