import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import './footer.css'

class Footer extends Component {
    render() {
        return (
            
            <div className="footer">
                <Link to={"/"} className="footerLogo">WalaBot</Link>
                <div className="footerNav">
                <Link to={'/shop'} className="footerText">Servicios</Link>
                <Link to={'/cotiza'} className="footerText">Cotiza</Link>
                </div>
            </div>
            
        )
    }
}
export default Footer;
