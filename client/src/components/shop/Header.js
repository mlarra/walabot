import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import './header.css'

class Header extends Component {
    render() {
        return (
            
            <div className="header">
                <Link to={"/"} className="headerLogo">WalaBot</Link>
                <div className="headerNav">
                <Link to={'/shop'} className="headerText">Servicios</Link>
                <Link to={'/cotiza'} className="headerText">Cotiza</Link>
                </div>
            </div>
            
        )
    }
}
export default Header;
