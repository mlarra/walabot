import React, { Component } from 'react'
import axios from 'axios/index';
import {withRouter} from 'react-router-dom';
import Cookies from 'universal-cookie';
import { v4 as uuid } from 'uuid';
import QuickReplies from './QuickReplies';

import Message from './Message';
import Card from './Card';

import './chatbot.css'

const cookies = new Cookies();

class Chatbot extends Component {

    messagesEnd;
    talkInput;
    constructor(props){
        super(props);

        this._handleInputKeyPress = this._handleInputKeyPress.bind(this);
        this._handleQuickReplyPayload = this._handleQuickReplyPayload.bind(this);
        this.hide = this.hide.bind(this);
        this.show = this.show.bind(this);
        this.state={
            messages:[],
            showBot: true,
            shopWelcomeSent: false,
            clientToken: false,
            regenerateToken: 0
        };

        if(cookies.get('userID') === undefined){
            cookies.set('userID', uuid(), {path:'/'});
            
        }
        
    }

    async df_text_query (text) {
        let says = {
            speaks: 'Yo',
            msg: {
                text : {
                    text: text
                }
            }
        }
        this.setState({ messages: [...this.state.messages, says]});
        
        // try {
        //     const res = await axios.post('/api/df_text_query',  {text, userID: cookies.get('userID')});

        //     for (let msg of res.data.fulfillmentMessages) {
        //         says = {
        //             speaks: 'WB',
        //             msg: msg
        //         }
        //         this.setState({ messages: [...this.state.messages, says]});
        //     }
        // } catch (e) {
        //     says = {
        //         speaks: 'WB',
                
        //         msg: {
        //             text : {
        //                 text: "Estoy con problemas, solucionaré los problemas pronto!"
        //             }
        //         }
        //     }
        //     this.setState({ messages: [...this.state.messages, says]});
        //     let that = this;
        //     setTimeout(function(){
        //         that.setState({ showBot: false})
        //     }, 2000);
        // }

        const request = {
            queryInput: {
                text: {
                    text: text,
                    languageCode: 'es-ES',
                },
            }
        };
        await this.df_client_call(request);
    };

        async df_event_query(event) {
            const request = {
                queryInput: {
                    event: {
                        name: event,
                        languageCode: 'es-ES',
                    },
                }
            };
    
            await this.df_client_call(request);
    
        };
    
        async df_client_call(request) {

            try {
                // const res = await axios.post('/api/df_event_query',  {event, userID: cookies.get('userID')});
    
                // for (let msg of res.data.fulfillmentMessages) {
                //     let says = {
                //         speaks: 'WB',
                //         msg: msg
                // if (process.env.REACT_APP_DIALOGFLOW_CLIENT_KEY === undefined
                //     || process.env.REACT_APP_GOOGLE_PROJECT_ID === undefined
                //     || process.env.REACT_APP_DF_SESSION_ID === undefined) {
                //     console.log('cant read from env variable');
                //     throw Error;
                    if (this.state.clientToken === false) {
                        const res = await axios.get('/api/get_client_token');
                        this.setState({clientToken: res.data.token});
                }

    
                var config = {
                    headers: {
                        //'Authorization': "Bearer " + process.env.REACT_APP_DIALOGFLOW_CLIENT_KEY,
                        'Authorization': "Bearer " + this.state.clientToken,
                        'Content-Type': 'application/json; charset=utf-8'

                    }
                };
    
                    //this.setState({ messages: [...this.state.messages, says]});
                    const res = await axios.post(
                        'https://dialogflow.googleapis.com/v2/projects/'+ process.env.REACT_APP_GOOGLE_PROJECT_ID +
                        '/agent/sessions/' + process.env.REACT_APP_DF_SESSION_ID + cookies.get('userID') + ':detectIntent',
                        request,
                        config
                    );
        
                    let  says = {};
        
        
                    if (res.data.queryResult.fulfillmentMessages ) {
                        for (let msg of res.data.queryResult.fulfillmentMessages) {
                            says = {
                                speaks: 'WB',
                                msg: msg
                            }
                            this.setState({ messages: [...this.state.messages, says]});
                        }
                }

            } catch (e) {
                if (e.response.status === 401 && this.state.regenerateToken < 1) {
                    this.setState({ clientToken: false, regenerateToken: 1 });
                    this.df_client_call(request);
                }
                else {
                    let says = {
                        speaks: 'WB',
                        msg: {
                            text : {
                                text: "I'm having troubles. I need to terminate. will be back later"}
                        }
                    }
                    this.setState({ messages: [...this.state.messages, says]});
                    let that = this;
                    setTimeout(function(){
                        that.setState({ showBot: false})
                    }, 2000);
                }
    

            }
    
        }


    show(event){
        event.preventDefault();
        event.stopPropagation();
        this.setState({showBot:true});
    }

    hide(event){
        event.preventDefault();
        event.stopPropagation();
        this.setState({showBot:false});
    }

    resolveAfterXSeconds(x){
        return new Promise(resolve => {
            setTimeout(()=>{
                resolve(x);
            }, x*1000);
        });
    }

    async componentDidMount(){
        this.df_event_query('Welcome');

        if(window.location.pathname === '/shop'){
            await this.resolveAfterXSeconds(2);
            this.df_event_query('WELCOME_SHOP');
            this.setState({shopWelcomeSent: true, showBot: true});
        }

        this.props.history.listen(()=> {
            console.log('listening');
            if(this.props.history.location.pathname === '/shop' && !this.state.shopWelcomeSent){
                this.df_event_query('WELCOME_SHOP');
                this.setState({shopWelcomeSent: true, showBot: true});
            }
        })
    }

    componentDidUpdate(){
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
        
        if ( this.talkInput ) {
            this.talkInput.focus();
        }
    }
    
    _handleQuickReplyPayload(event, payload, text){
        event.preventDefault();
        event.stopPropagation();

        switch (payload) {
            case 'recommend_yes':
                this.df_event_query('SHOW_RECOMMENDATIONS')
                break;
            case 'training_masterclass':
                this.df_event_query('MASTERCLASS');
                break;
            default:
                this.df_text_query(text);
                break;
        }
    }

    renderMessages(returnedMessages){
        if(returnedMessages){
            return returnedMessages.map((message, i) => {
                return this.renderOneMessage(message, i); 
            });
            
        } else{
            return null;
        }
    }

    _handleInputKeyPress(e){
        if(e.key === 'Enter'){
            this.df_text_query(e.target.value);
            e.target.value = '';
        }
    }

    renderCards(cards) {
        // return cards.map((card, i) => <Card key={i} payload={card.structValue}/>);
        return cards.map((card, i) => <Card key={i} payload={card}/>);
    }

    renderOneMessage(message, i) {

        if (message.msg && message.msg.text && message.msg.text.text) {
            return <Message key={i} speaks={message.speaks} text={message.msg.text.text}/>;
        //} else if (message.msg && message.msg.payload.fields.cards) { //message.msg.payload.fields.cards.listValue.values
        } else if (message.msg
        && message.msg.payload
        && message.msg.payload.cards) { //message.msg.payload.fields.cards.listValue.values 
        // else if (message.msg && message.msg.payload
        //             && message.msg.payload.fields && message.msg.payload.fields.cards) { //message.msg.payload.fields.cards.listValue.values
            return <div key={i}>
                <div className="card-panel grey lighten-5 z-depth-1">
                    <div style={{overflow: 'hidden'}}>
                        <div className="col s2">
                            <a href="/" className="btn-floating btn-large waves-effect waves-light brown">{message.speaks}</a>
                        </div>
                        <div style={{ overflow: 'auto', overflowY: 'scroll'}}>
                            {/* <div style={{ height: 300, width:message.msg.payload.fields.cards.listValue.values.length * 270}}>
                                {this.renderCards(message.msg.payload.fields.cards.listValue.values)} */}
                                <div style={{ height: 300, width:message.msg.payload.cards.length * 270}}>
                                {this.renderCards(message.msg.payload.cards)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    } else if (message.msg &&
                        message.msg.payload &&
                       // message.msg.payload.fields &&
                       // message.msg.payload.fields.quick_replies
                       message.msg.payload.quick_replies
                    ) {
                        return <QuickReplies
                            text={message.msg.payload.text ? message.msg.payload.text : null}
                            //text={message.msg.payload.fields.text ? message.msg.payload.fields.text : null}
                            key={i}
                            replyClick={this._handleQuickReplyPayload}
                            speaks={message.speaks}
                            payload={message.msg.payload.quick_replies}/>;
                            //payload={message.msg.payload.fields.quick_replies.listValue.values}/>;
        }
    }

    render() {
        if (this.state.showBot) {
            return (
                <div className="chatNav">
                    
                        <div className="chatheader">
                            <div className="chatHeaderLogo">WB</div>
                            
                            <div className="chatHeaderNav">
                                <a className="headerText" href="/" onClick={this.hide}>Cerrar</a></div>
                            
                            </div>
                 

                <div id="chatbot"  style={{ minHeight: 388, maxHeight: 388, width:'100%', overflow: 'auto', minWidth:400}}>
                {this.renderMessages(this.state.messages)}
                        <div ref={(el) => { this.messagesEnd = el; }}
                             style={{ float:"left", clear: "both" }}>
                        </div>
                    </div>
                    <div className=" col s12" >
                        <input style={{margin: 0, paddingLeft: '1%', paddingRight: '1%', width: '98%'}} ref={(input) => { this.talkInput = input; }} placeholder="Como te puedo ayudar?"  onKeyPress={this._handleInputKeyPress} id="user_says" type="text" />
                    </div>

                </div>
            );
        } else {
            return (
                <div style={{ minHeight: 40, maxHeight: 500, width:400, position: 'fixed', bottom: 0, right: 0}}>
                    
                        <div className="chatheader">
                            <a href="/" className="chatHeaderLogo">WB</a>
                            <div className="chatHeaderNav">
                                <a className="headerText" href="/" onClick={this.show}>Mostrar</a>
                            </div>
                        </div>
                    
                    <div ref={(el) => { this.messagesEnd = el; }}
                         style={{ float:"left", clear: "both" }}>
                    </div>
                </div>
                         );
        }
}
}

export default withRouter(Chatbot);
