import React, { Component } from 'react'

class Message extends Component {
    render() {
        return (
            <div className="col s12 m8 offset-m2 offset-l3">
                <div className="card-panel grey lighten-5 z-depth-1">
                    <div className="row valign-wrapper">
                        {this.props.speaks === 'WB' && 
                            <div className="col s2">
                            <div className="btn-floating btn-large waves-effect waves-light brown">{this.props.speaks}<i className="material-icons">add</i></div>
                            </div>
                        }
                    <div className="col s10">
                        <span className="black-text">
                            {this.props.text}
                        </span>
                    </div>

                    {this.props.speaks === 'Yo' && 
                            <div className="col s2">
                            <div className="btn-floating btn-large waves-effect waves-dark green">{this.props.speaks}<i className="material-icons">add</i></div>
                            </div>
                        }
                    </div>

                </div>
                
            </div>
        )
    }
}


export default Message