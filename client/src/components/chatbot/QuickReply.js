import React, { Component } from 'react'

 class QuickReply extends Component {
    render() {
        // if(this.props.reply.structValue.fields.payload){
        if (this.props.reply.payload) {
            return (

                <a style={{margin:3}} href="/" className="btn-floating btn-large waves-effect waves-light blue" onClick={(event)=> this.props.click(event, this.props.reply.payload, this.props.reply.text)}>
                    {/* {this.props.reply.structValue.fields.text.stringValue} */}
                    {this.props.reply.text}
                    </a>
                )
        } else{
            return (

                // <a href={this.props.reply.structValue.fields.link.stringValue}
                <a style={{ margin: 3}} href={this.props.reply.link}
                 className="btn-floating btn-large waves-effect waves-light blue">
                    {/* {this.props.reply.structValue.fields.text.stringValue} */}
                    {this.props.reply.text}
                    </a>
                )

        }

    }
}

export default QuickReply;