import React, { Component } from 'react'

class Card extends Component {
    render() {
        return (
            <div  style={{ height: 270, paddingRight:30, float: 'left'}}>
            <div className="card">
                <div className="card-image" style={{ width: 240}}>
                    {/* <img alt={this.props.payload.fields.header.stringValue} src={this.props.payload.fields.image.stringValue} />
                    <span className="card-title">{this.props.payload.fields.header.stringValue}</span> */}
                     <img alt={this.props.payload.header} src={this.props.payload.image} />
                    <span className="card-title">{this.props.payload.header}</span>
                </div>
                <div className="card-content">
                    {/* {this.props.payload.fields.description.stringValue}
                    <p> <a href="/">{this.props.payload.fields.price.stringValue}</a></p> */}
                    {this.props.payload.description}
                    <p> <a href="/">{this.props.payload.price}</a></p>
                </div>
                <div className="card-action">
                    {/* <a target="_blank" rel="noopener noreferrer" href={this.props.payload.fields.link.stringValue}>VER</a> */}
                    <a target="_blank" rel="noopener noreferrer" href={this.props.payload.link}>VER</a>
                </div>
            </div>
        </div>
        )
    }
}

export default Card;